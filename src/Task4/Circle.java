package Task4;

public class Circle {
    private int id;
    private double radius;
    private double diameter;
    private double area;
    private double perimeter;

    public Circle(int id, double radius, double diameter) {
        this.id = id;
        this.radius = radius;
        this.diameter = diameter;

        if (this.radius > 0) {
            area =  Math.PI * Math.pow(this.radius, 2);
        } else if (this.diameter > 0){
            area = (Math.PI / 4) * Math.pow(this.diameter, 2);
        } else {
            area = 0;
        }

        if (this.radius > 0) {
            perimeter = 2 * Math.PI * this.radius;
        } else if (this.diameter > 0){
            perimeter = Math.PI * this.diameter;
        } else {
            perimeter = 0;
        }
    }

    @Override
    public String toString() {
        return "Окружность[" + id + "]" +
                "\n\tРадиус = " + radius +
                "\n\tДиаметр = " + diameter +
                "\n\tПлощадь = " + area +
                "\n\tПериметр = " + perimeter +
                "\n}\n";
    }

    public void updateArea() {
        if (this.radius > 0) {
            area =  Math.PI * Math.pow(this.radius, 2);
        } else if (this.diameter > 0){
            area = (Math.PI / 4) * Math.pow(this.diameter, 2);
        } else {
            area = 0;
        }
    }

    public void updatePerimeter() {
        if (this.radius > 0) {
            perimeter = 2 * Math.PI * this.radius;
        } else if (this.diameter > 0){
            perimeter = Math.PI * this.diameter;
        } else {
            perimeter = 0;
        }
    }

    public double getArea() {
        return area;
    }

    public double getPerimeter() {
        return perimeter;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }
}
