import Task1.Student;
import Task2.Car;
import Task3.Customer;
import Task4.Circle;

import java.text.SimpleDateFormat;
import java.util.*;

public class Block2 {

    public static void main(String[] args) {
        //Task1();
        //Task2();
        //Task3();
        //Task4();
    }

    public static void Task1() {
        System.out.println("Task1");

        List<Student> students = new ArrayList<>();
        students.add(new Student(1, "Ставский", "Илья", "Дмитриевич",
                "20.09.1998", "пр. Дзержинского 33/1, кв 3.", "89068347184",
                "Программирование в компьютерных системах", 4, "4пк1"));
        students.add(new Student(2, "Старецв", "Сергей", "Дмитриевич",
                "03.12.1997", "ул. Пушкина 12, кв 15.", "89068237185",
                "Прикладная математика и информатика", 4, "4пи1"));

        //a)
        System.out.println("\na)");
        System.out.print("Введите факультет: ");
        Scanner in = new Scanner(System.in);
        try {
            String faculty = in.nextLine();
            for (Student student : students) {
                if (student.getFaculty().equalsIgnoreCase(faculty)) {
                    System.out.println(student.toString());
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        //b)
        System.out.println("\nb)");
        List<String> faculties = new ArrayList<>();
        for (Student student : students) {
            if (!faculties.contains(student.getFaculty())) {
                faculties.add(student.getFaculty());
            }
        }

        for (String faculty : faculties) {
            System.out.println("Факультет: " + faculty);
            for (Student student : students) {
                if (student.getFaculty().equalsIgnoreCase(faculty))
                    System.out.println(student.toString());
            }
        }

        List<Integer> courses = new ArrayList<>();
        for (Student student : students) {
            if (!courses.contains(student.getCourse())) {
                courses.add(student.getCourse());
            }
        }

        for (Integer course : courses) {
            System.out.println("Курс " + course + ":");
            for (Student student : students) {
                if (student.getCourse() == course)
                    System.out.println(student.toString());
            }
        }

        //c)
        System.out.println("\nc)");
        try {
            System.out.print("Введите год: ");
            int tempYear = in.nextInt();
            String year = "" + (tempYear + 1);
            Date tempDate = new SimpleDateFormat("yyyy").parse(year);
            for (Student student : students) {
                if (student.getDataBirthday().after(tempDate)) {
                    System.out.println(student.toString());
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        //d)
        System.out.println("\nd)");
        List<String> groups = new ArrayList<>();
        for (Student student : students) {
            if (!groups.contains(student.getGroup())) {
                groups.add(student.getGroup());
            }
        }

        for (String group : groups) {
            System.out.println("Группа " + group + ":");
            for (Student student : students) {
                if (student.getGroup().equalsIgnoreCase(group)) {
                    System.out.println(student.toString());
                }
            }
        }

        in.close();
    }

    public static void Task2() {
        System.out.println("Task2;");
        List<Car> cars = new ArrayList<Car>();
        Collections.addAll(cars,
                new Car(1, "Toyota", "Camry", 2010, "Black", 1_500_000, 228),
                new Car(2, "Toyota", "Corolla", 2003, "Gray", 1_000_000, 322),
                new Car(3, "BMW", "X5", 2005, "Blue", 4_000_000, 172),
                new Car(4, "KIA", "Optima", 2016, "Black", 1_709_000, 684));

        Scanner in = new Scanner(System.in);

        //a)
        try {
            System.out.println("a)");
            System.out.print("Введите модель автомобиля: ");
            String model = in.nextLine();

            for (Car car : cars) {
                car.findBrand(model);
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        //b)
        try {
            System.out.println("\nb)");
            System.out.print("Введите модель автомобиля: ");
            String model = in.nextLine();

            System.out.print("Введите кол-во лет эсплуатации: ");
            int year = in.nextInt();

            for (Car car : cars) {
                car.findBrand(model, year);
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        //c)
        try {
            System.out.println("\nc)");
            System.out.print("Введите год выпуска: ");
            int year = in.nextInt();

            System.out.print("Введите цену: ");
            int price = in.nextInt();

            for (Car car : cars) {
                car.findCar(year, price);
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        in.close();
    }

    public static void Task3() {
        System.out.println("\nTask3;");
        System.out.println("a)");
        List<Customer> customers = new ArrayList<Customer>();
        Collections.addAll(customers,
                new Customer(1, "Ставский", "Илья", "Дмитриевич", "пр. Дзержинского 33/1, кв. 3", 1111_1111_1111_1111L),
                new Customer(2, "Старцев", "Сергей", "Дмитриевич", "ул. Пушкина 3, кв. 12", 2222_2222_2222_2222L),
                new Customer(3, "Бессонов", "Дмитрий", "Олегович", "ул. Пушкина 31, кв. 21", 3333_3333_3333_3333L));

        customers.stream()
                .sorted((c1, c2) -> c1.getSurname().compareToIgnoreCase(c2.getSurname()))
                .forEach(c -> System.out.println(c.toString()));

        System.out.println("\nb)");
        Scanner in = new Scanner(System.in);

        System.out.print("Введите начало диапазона: ");
        long cardStart = in.nextLong();

        System.out.print("Введите конец диапазона: ");
        long cardEnd = in.nextLong();

        customers.stream()
                .filter(c -> c.getCreditCard() >= cardStart && c.getCreditCard() <= cardEnd)
                .forEach(c -> System.out.println(c.toString()));
        in.close();
    }

    public static void Task4() {
        System.out.println("\nTask4;");
        List<Circle> circles = new ArrayList<Circle>();
        Collections.addAll(circles,
                new Circle(1, 5, 10),
                new Circle(2, 3.5, 7),
                new Circle(3, 4, 8));

        Circle max = circles.stream().max(Comparator.comparing(c -> c.getArea())).get();
        Circle min = circles.stream().min(Comparator.comparing(c -> c.getArea())).get();

        System.out.println("Окружность с минимальным радиусом (периметром): \n" + min.toString());
        System.out.println("Окружность с максимальным радиусом (периметром): \n" + max.toString());
    }
}
