package Task1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Student {
    private int id;
    private String surname;
    private String name;
    private String patronymic;
    private Date dateBirthday;
    private String address;
    private String phoneNumber;
    private String faculty;
    private int course;
    private String group;

    public Student(int id, String surname, String name, String patronymic, String dateBirthday, String address, String phoneNumber, String faculty, int course, String group) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        try {
            this.dateBirthday = new SimpleDateFormat("dd.MM.yyyy").parse(dateBirthday);
        } catch (ParseException ex) {
            System.out.println("Parse Exception");
        }
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.faculty = faculty;
        this.course = course;
        this.group = group;
    }

    @Override
    public String toString() {
        return "Студент[" + id + "]" +
                "\n\tФамилия='" + surname + '\'' +
                "\n\tИмя='" + name + '\'' +
                "\n\tОтчество='" + patronymic + '\'' +
                "\n\tДата рождения=" + new SimpleDateFormat("dd.MM.yyyy").format(dateBirthday) +
                "\n\tАдрес='" + address + '\'' +
                "\n\tНомер='" + phoneNumber + '\'' +
                "\n\tФакультет='" + faculty + '\'' +
                "\n\tКурс=" + course +
                "\n\tГруппа='" + group + '\'';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Date getDataBirthday() {
        return dateBirthday;
    }

    public void setDataBirthday(Date dataBirthday) {
        this.dateBirthday = dataBirthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}