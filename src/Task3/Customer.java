package Task3;

public class Customer {
    private int id;
    private String surname;
    private String name;
    private String patronymic;
    private String address;
    private long creditCard;

    public Customer(int id, String surname, String name, String patronymic, String address, long creditCard) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.address = address;
        this.creditCard = creditCard;
    }

    @Override
    public String toString() {
        return "Клиент[" + id + "] {" +
                "\n\tФамилия = '" + surname + '\'' +
                "\n\tИмя = '" + name + '\'' +
                "\n\tОтчество = '" + patronymic + '\'' +
                "\n\tАдрес = '" + address + '\'' +
                "\n\tНомер кредитной карты = " + creditCard +
                "\n}\n";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(long creditCard) {
        this.creditCard = creditCard;
    }
}
