package Task2;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Car {
    private int id;
    private String brand;
    private String model;
    private int year;
    private String color;
    private double price;
    private int registrationNumber;

    public Car(int id, String brand, String model, int year, String color, double price, int registrationNumber) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.color = color;
        this.price = price;
        this.registrationNumber = registrationNumber;
    }

    @Override
    public String toString() {
        return "Автомобиль[" + id + "] {" +
                "\n\tМарка = '" + brand + '\'' +
                "\n\tМодель = '" + model + '\'' +
                "\n\tГод выпуска = " + year +
                "\n\tЦвет = '" + color + '\'' +
                "\n\tЦена = " + price +
                "\n\tРегистрационный номер = " + registrationNumber +
                "\n}";
    }

    public void findBrand(String brand) {
        if (this.brand.equalsIgnoreCase(brand))
            System.out.println(toString());
    }

    public void findBrand(String brand, int n) {
        if (this.brand.equalsIgnoreCase(brand) && n < (Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date())) - this.year)) {
            System.out.println(toString());
        }
    }

    public void findCar(int year, double price) {
        if (this.year == year && this.price > price) {
            System.out.println(toString());
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(int registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
}
